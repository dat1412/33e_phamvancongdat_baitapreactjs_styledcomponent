import "./App.css";
import DemoJSS from "./JSS_StyledComponents/DemoJSS/DemoJSS";
import DemoTheme from "./JSS_StyledComponents/Themes/DemoTheme";
import ToDoList from "./JSS_StyledComponents/ToDoList/ToDoList";

function App() {
  return (
    <div className="App">
      {/* <DemoJSS /> */}
      {/* <DemoTheme /> */}
      <ToDoList />
    </div>
  );
}

export default App;
