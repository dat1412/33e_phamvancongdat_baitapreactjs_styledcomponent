import styled, { ThemeProvider } from "styled-components";
import React, { Component } from "react";

const configDarkTheme = {
  background: "#000",
  color: "#ffffff",
  fontSize: "15px",
  fontWeight: "300",
};

const configLightTheme = {
  background: "#6633ff",
  color: "#ffffff",
  fontSize: "20px",
  fontWeight: "600",
};

const DivStyle = styled.div`
  background-color: ${(props) => props.theme.background};
  color: ${(props) => props.theme.color};
  font-size: ${(props) => props.theme.fontSize};
  font-weight: ${(props) => props.theme.fontWeight};
  padding: 16px;
  width: 50px;
  margin: 0 auto;
`;

export default class DemoTheme extends Component {
  state = {
    currentTheme: configDarkTheme,
  };

  handleChangTheme = (e) => {
    this.setState({
      currentTheme: e.target.value == 1 ? configDarkTheme : configLightTheme,
    });
  };
  render() {
    return (
      <ThemeProvider theme={this.state.currentTheme}>
        <DivStyle>123</DivStyle>
        <select onChange={this.handleChangTheme}>
          <option value="1">Dark theme</option>
          <option value="2">Light theme</option>
        </select>
      </ThemeProvider>
    );
  }
}
