import {
  CHANGE_THEME,
  ADD_TASK,
  REMOVE_TASK,
  DONE_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from "../constants/constantToDoLits";

export const handleChangeThemeAction = (theme) => {
  return {
    type: CHANGE_THEME,
    payload: theme,
  };
};

export const handleAddTaskAction = (newTask) => {
  return {
    type: ADD_TASK,
    payload: newTask,
  };
};

export const handleRemoveTaskAction = (idTask) => {
  return {
    type: REMOVE_TASK,
    payload: idTask,
  };
};

export const handleDoneTaskAction = (idTask) => {
  return {
    type: DONE_TASK,
    payload: idTask,
  };
};

export const handleEditTaskAction = (task) => {
  return {
    type: EDIT_TASK,
    payload: task,
  };
};

export const handleUpdateTaskAction = (taskName) => {
  return {
    type: UPDATE_TASK,
    payload: taskName,
  };
};
