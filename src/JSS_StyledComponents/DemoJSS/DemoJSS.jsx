import React, { Component } from "react";
import { Button } from "../Components/Button";
import { StyledLink } from "../Components/Link";
import { TextField } from "../Components/TextField";

export default class DemoJSS extends Component {
  render() {
    console.log("Button: ", Button);
    return (
      <div>
        <Button bgBlue fontSize2X>
          Click me
        </Button>

        <StyledLink>Go to home</StyledLink>

        <TextField textColor="blue" />
      </div>
    );
  }
}
