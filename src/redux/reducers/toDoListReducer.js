import { ToDoListDarkTheme } from "../../JSS_StyledComponents/Themes/ToDoListDarkTheme";
import {
  ADD_TASK,
  CHANGE_THEME,
  DONE_TASK,
  EDIT_TASK,
  REMOVE_TASK,
  UPDATE_TASK,
} from "../constants/constantToDoLits";

let initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    {
      id: 1,
      taskName: "task 1",
      status: false,
    },
    {
      id: 2,
      taskName: "task 2",
      status: true,
    },
    {
      id: 3,
      taskName: "task 3",
      status: false,
    },
    {
      id: 4,
      taskName: "task 4",
      status: true,
    },
  ],
  taskEdit: {},
};

export const toDoListReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CHANGE_THEME: {
      return { ...state, themeToDoList: payload };
    }
    case ADD_TASK: {
      if (payload.taskName.trim() === "") {
        alert("Hãy nhập việc cần làm !");
        return { ...state };
      }
      let index = state.taskList.findIndex((task) => {
        return task.taskName === payload.taskName;
      });
      if (index !== -1) {
        alert("Việc cần làm đã tồn tại !");
        return { ...state };
      }
      let cloneTaskList = [...state.taskList];
      cloneTaskList.push(payload);
      state.taskList = cloneTaskList;
      return { ...state };
    }
    case REMOVE_TASK: {
      let index = state.taskList.findIndex((task) => {
        return task.id === payload;
      });
      if (index === -1) return { ...state };
      let cloneTaskList = [...state.taskList];
      cloneTaskList.splice(index, 1);
      state.taskList = cloneTaskList;
      return { ...state };
    }

    case DONE_TASK: {
      let index = state.taskList.findIndex((task) => {
        return task.id === payload;
      });
      if (index === -1) return { ...state };
      let cloneTaskList = [...state.taskList];
      cloneTaskList[index].status = true;
      state.taskList = cloneTaskList;
      return { ...state };
    }
    case EDIT_TASK: {
      return { ...state, taskEdit: payload };
    }
    case UPDATE_TASK: {
      state.taskEdit = { ...state.taskEdit, taskName: payload };

      let index = state.taskList.findIndex((task) => {
        return task.id === state.taskEdit.id;
      });
      if (index === -1) return { ...state };

      let cloneTaskList = [...state.taskList];
      cloneTaskList[index] = state.taskEdit;
      state.taskList = cloneTaskList;

      state.taskEdit = {
        id: -1,
        taskName: "",
        status: false,
      };

      return { ...state };
    }
    default:
      return { ...state };
  }
};
