import { ToDoListDarkTheme } from "./ToDoListDarkTheme";
import { ToDoListLightTheme } from "./ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "./ToDoListPrimaryTheme";

export let arrTheme = [
  {
    id: "1",
    name: "Dark theme",
    theme: ToDoListDarkTheme,
  },
  {
    id: "2",
    name: "Primary theme",
    theme: ToDoListPrimaryTheme,
  },
  {
    id: "3",
    name: "Light theme",
    theme: ToDoListLightTheme,
  },
];
