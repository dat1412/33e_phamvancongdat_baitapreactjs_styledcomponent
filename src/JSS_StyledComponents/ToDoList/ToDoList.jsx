import React, { Component } from "react";
import { ThemeProvider } from "styled-components";
import { Container } from "../Container/Cotainer";
import { Dropdown } from "../Components/Dropdown";
import { Heading1, Heading2, Heading3, Heading4 } from "../Components/Heading";
import { TextField } from "../Components/TextField";
import { Button } from "../Components/Button";
import { Table, Tbody, Td, Th, Thead, Tr } from "../Components/Table";

import { arrTheme } from "../Themes/ThemeManager";

import { connect } from "react-redux";
import {
  handleAddTaskAction,
  handleChangeThemeAction,
  handleDoneTaskAction,
  handleEditTaskAction,
  handleRemoveTaskAction,
  handleUpdateTaskAction,
} from "../../redux/actions/actionToDoList";

class ToDoList extends Component {
  state = {
    taskName: "",
    isDisabled: true,
  };

  // life cycle phien ban < 16
  // componentWillReceiveProps(newProps) {
  //   if (newProps.taskEdit.taskName) {
  //     this.setState({
  //       taskName: newProps.taskEdit.taskName,
  //     });
  //   }
  // }

  // Life cycle phien ban moi
  // Ko truy xuat dc tro this do la Life cycle tinh~
  // static getDerivedStateFromProps(newProps, currentState) {
  //   // newProps la props moi
  //   // currentState ung voi state hien tai l this.state
  //   // Tra ve state moi
  //   // let newState = { ...currentState };
  //   // newState.taskName = newProps.taskEdit.taskName;
  //   // return newState;
  //   // Khi return null thi state giu nguyen
  //   // return null;
  // }

  renderTaskToDo = () => {
    return this.props.taskList.map((task, index) => {
      if (!task.status) {
        return (
          <Tr key={index}>
            <Th>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.setState(
                    {
                      isDisabled: false,
                    },
                    () => {
                      this.props.handleEditTask(task);
                    }
                  );
                }}
              >
                <i className="fa fa-edit"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.hanldeDoneTask(task.id);
                }}
              >
                <i className="fa fa-check"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.handleRemoveTask(task.id);
                }}
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      }
    });
  };
  renderTaskCompleted = () => {
    return this.props.taskList.map((task, index) => {
      if (task.status) {
        return (
          <Tr key={index}>
            <Th>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.handleRemoveTask(task.id);
                }}
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      }
    });
  };
  handleChangeInput = (e) => {
    let { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  };

  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return (
        <option key={index} value={theme.id}>
          {theme.name}
        </option>
      );
    });
  };

  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;
              let index = arrTheme.findIndex((theme) => {
                return theme.id === value;
              });
              if (index === -1) return;
              let themeCurrent = arrTheme[index].theme;
              this.props.hanldeChangeTheme(themeCurrent);
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading2>To Do List</Heading2>
          <TextField
            onChange={this.handleChangeInput}
            name="taskName"
            label="Task name"
            value={this.state.taskName}
          ></TextField>
          <Button
            onClick={() => {
              let newTask = {
                id: Date.now(),
                taskName: this.state.taskName.trim(),
                status: false,
              };
              this.props.handleAddTask(newTask);
              this.setState({
                taskName: "",
              });
            }}
            className="ml-2"
          >
            <i className="fa fa-plus"></i>
            <span className="ml-1">Add task</span>
          </Button>
          {this.state.isDisabled ? (
            <Button
              disabled
              onClick={() => {
                this.props.handleUpdateTask(this.state.taskName);
              }}
              className="ml-2"
            >
              <i className="fa fa-upload"></i>
              <span className="ml-1">Update task</span>
            </Button>
          ) : (
            <Button
              onClick={() => {
                let { taskName } = this.state;
                this.setState(
                  {
                    taskName: "",
                    isDisabled: true,
                  },
                  () => {
                    this.props.handleUpdateTask(taskName);
                  }
                );
              }}
              className="ml-2"
            >
              <i className="fa fa-upload"></i>
              <span className="ml-1">Update task</span>
            </Button>
          )}
          <hr />
          <Heading3>Task To Do</Heading3>
          <Table>
            <Thead>{this.renderTaskToDo()}</Thead>
          </Table>
          <Heading3>Task Completed</Heading3>
          <Table>
            <Thead>{this.renderTaskCompleted()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }

  // Lyfe cycle chay sau khi render
  componentDidUpdate(prevProps, prevState) {
    if (this.props.taskEdit.id !== prevProps.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
    }
    // ham nay se tra ve props cu va state cu truoc khi render
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.toDoListReducer.themeToDoList,
    taskList: state.toDoListReducer.taskList,
    taskEdit: state.toDoListReducer.taskEdit,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    hanldeChangeTheme: (theme) => {
      dispatch(handleChangeThemeAction(theme));
    },
    handleAddTask: (newTask) => {
      dispatch(handleAddTaskAction(newTask));
    },
    handleRemoveTask: (idTask) => {
      dispatch(handleRemoveTaskAction(idTask));
    },
    hanldeDoneTask: (idTask) => {
      dispatch(handleDoneTaskAction(idTask));
    },
    handleEditTask: (task) => {
      dispatch(handleEditTaskAction(task));
    },
    handleUpdateTask: (taskName) => {
      dispatch(handleUpdateTaskAction(taskName));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDoList);
